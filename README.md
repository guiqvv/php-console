# php-console

php-console是一个极简的PHP在线调试利器。源自于实际的项目需要。

# Git
> https://git.oschina.net/guiqvv/php-console

# 要求
> PHP >= 5.4 或 PHP >= 7.0
> 谷歌浏览器 或 火狐浏览器

# 安装
> composer require zendforum/php-console
或者
require_once "/path-to/PhpConsole.php";

# 使用
``` php
use zendforum\PhpConsole\Debug;

Debug::log('hello,world');
Debug::log(array('hello','world'));
Debug::log('hello', 'flag1');
Debug::log('world', 'flag1');
Debug::log(array('hello'), 'flag2');
Debug::log(array('world'), 'flag2');

Debug::echo();
```
### 参数说明
> Debug::log($data, $group)
$data:可以是任何数据类型（除 resource 类型之外） 
$group:分组标识

在浏览器打开上面的文件(需要在网页地址后面增加get参数:pckey=helloworld,如:domain.com?pckey=helloworld);页面点击右键 -> 检查 -> Console ;即可看到上面的调试信息。
注:pckey为通行码,可以自己设置

# 更新记录
### 0.1.1
* 参数优化、文档补充

### 0.1.0
* 初始化
