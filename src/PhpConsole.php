<?php
/**
 * A useful console debug tool for PHP
 */
namespace zendforum\PhpConsole;

/**
 * 功能开关
 */
define('PHP_CONSOLE_ENABLE', true);

/**
 * PHP console debug tool
 */
class Debug {

    //pckey:通行码,可以自己设置
    const PASSPHRASE = 'helloworld';

    //记录器
    private static $logger = [];

    /**
     * @param mixed $data 需要记录的数据,可以为任何数据类型（除 resource 类型之外）
     * @param string $group 分组标识
     */
    public static function log ($data, $group = '') {
        if (isset($group) && $group != '') self::$logger["$group"][] = $data;
        else self::$logger[] = $data;
    }

    /**
     * 打印输出
     */
    public static function echo () {
        if (!defined('PHP_CONSOLE_ENABLE') || empty(PHP_CONSOLE_ENABLE) || !self::_is_authorized()) return;

        //默认数据
//        self::log();

        echo self::_eol() . '<script>';
        echo self::_output();
        echo '</script>' . self::_eol();
    }

    private static function _output () {
        if (!empty(self::$logger)) {
            foreach (self::$logger as $k => $v) {
                if (is_string($k)) {
                    echo "console.group('{$k}');";
                    foreach ($v as $vv) {
                        echo "console.debug('" . json_encode($vv) . "');";
                    }
                    echo "console.groupEnd();";
                }
                else {
                    echo "console.debug('" . json_encode($v) . "');";
                }
            }
        }
    }

    private static function _eol () {
        return (php_sapi_name() === 'cli') ? PHP_EOL : '<br>';
    }

    private static function _passphrase_check ($passphrase) {
        if (empty($passphrase) || $passphrase != self::PASSPHRASE) return false;

        return true;
    }

    /**
     * 认证
     */
    private static function _is_authorized () {
        $passphrase = !empty($_GET['pckey']) ? trim($_GET['pckey']) : '';

        return self::_passphrase_check($passphrase);
    }

}
